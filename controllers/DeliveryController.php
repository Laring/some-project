<?php

namespace crm\modules\angular_api\controllers;

use common\models\order\Order;
use common\services\delivery\DeliveryException;
use crm\services\delivery\DeliveryFactory;
use crm\services\delivery\DeliveryService;
use yii\base\Exception;
use Yii;

class DeliveryController extends BehaviorController
{
    /*
     * ...
     */

    public function actionSend()
    {
        $delivery_type = Yii::$app->request->post('delivery_type');
        $credentials = Yii::$app->request->post('credentials');
        $orders_id_array = Yii::$app->request->post('orders');

        $delivery = DeliveryFactory::createDelivery($delivery_type);
        $deliveryService = new DeliveryService($delivery);

        $success = [];
        $failed = [];
        foreach ($orders_id_array as $order_id) {
            try {
                if (!$order = Order::findOne(['order_id' => $order_id])) {
                    $failed[$order_id] = 'OrderNotFound';
                    continue;
                }
                $deliveryService->execute($order, $credentials);
                $success[] = ['order_id' => $order->order_hash];
            } catch (DeliveryException $e) {
                $failed[] = ['order_id' => $order->order_hash, 'message' => $e->getMessage()];
            } catch (Exception $e) {
                $failed[] = ['order_id' => $order->order_hash, 'message' => $e->getMessage()];
            }
        }
        $this->response->data['success_orders'] = $success;
        $this->response->data['failed_orders'] = $failed;
        $this->response->send();
    }

    /*
     * ...
     */
}