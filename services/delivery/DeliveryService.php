<?php

namespace crm\services\delivery;

use common\models\delivery\DeliveryApi;
use common\models\delivery\OrderDelivery;
use common\models\delivery\UserDeliveryApi;
use common\models\order\Order;
use common\models\order\OrderStatus;
use common\services\delivery\DeliveryException;
use common\services\order\OrderCommonService;
use common\services\ValidateException;
use yii\base\Exception;
use Yii;

class DeliveryService
{
    /**
     * @var DeliveryApiInterface
     */
    private $delivery;
    /**
     * @var OrderCommonService
     */
    private $orderService;

    /**
     * DeliveryService constructor.
     * @param DeliveryApiInterface $delivery
     */
    public function __construct(DeliveryApiInterface $delivery)
    {
        $this->orderService = new OrderCommonService();
        $this->delivery = $delivery;
    }

    /**
     * @param Order $order
     * @param $credentials
     * @return bool
     * @throws Exception
     */
    public function execute(Order $order, $credentials)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ($response = $this->delivery->send($order, $credentials)) {
                $this->orderService->changeStatus($order, OrderStatus::DELIVERY_IN_PROGRESS, [
                    'declaration' => $response['track_number']
                ]);
            }

            if (!$this->saveOrderDelivery($response, $order))
                throw new DeliveryException("Failed to save delivery data for Order #$order->order_hash");

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return true;
    }

    /**
     * @param $response
     * @param $order
     * @return bool
     * @throws ValidateException
     */
    private function saveOrderDelivery($response, $order)
    {
        $deliveryApi = DeliveryApi::find()->where(['api_name' => $this->delivery->getClassName()])->one();
        $userApi = UserDeliveryApi::find()->where([
            'delivery_api_id' => $deliveryApi->getPrimaryKey(),
            'advert_id' => Yii::$app->user->getId(),
            'country_id' => $order->customer->country_id
        ])->one();

        $orderDelivery = new OrderDelivery();
        $orderDelivery->setAttributes([
            'order_id' => $order->order_id,
            'order_hash' => $order->order_hash,
            'offer_id' => $order->offer_id,
            'offer_hash' => $order->offer->offer_hash,
            'sent_by' => Yii::$app->user->getId(),
            'delivery_api_id' => $deliveryApi->getPrimaryKey(),
            'user_api_id' => $userApi->getPrimaryKey(),
            'track_number' => $response['track_number'],
            'remote_status' => $response['remote_status'],
            'shipment_data' => $response['shipment_data']
        ]);

        if (!$orderDelivery->validate())
            throw new ValidateException($orderDelivery->errors);

        return $orderDelivery->save();
    }
}