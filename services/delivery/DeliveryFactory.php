<?php

namespace crm\services\delivery;

use crm\services\delivery\fetchr\Dropship;
use crm\services\delivery\fetchr\Fulfillment;

class DeliveryFactory
{
    private static const DELIVERIES = [
        'dropship' => Dropship::class,
        'fulfillment' => Fulfillment::class
    ];

    public static function createDelivery($type)
    {
        $class = self::DELIVERIES[$type];
        if (!$class)
            throw new \InvalidArgumentException("Delivery $type not found");
        return new $class;
    }
}