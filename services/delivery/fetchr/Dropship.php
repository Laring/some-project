<?php

namespace crm\services\delivery\fetchr;

use common\models\order\Order;
use common\services\delivery\DeliveryException;
use crm\services\delivery\DeliveryApiInterface;
use linslin\yii2\curl\Curl;

/**
 * Class Dropship
 *
 * @package crm\services\delivery\fetchr
 */
class Dropship implements DeliveryApiInterface
{
    private static const METHOD_CREATE_ORDER = 'create_orders';
    private static const USERNAME = 'username';
    private static const PASSWORD = 'password';
    private static const PICKUP_LOCATION = 'Kiev';
    private static const PICKUP_COUNTRY = 'Ukraine';
    private static const PICKUP_CITY = 'Kiev';
    private static const URL = 'http://ibash.org.ru/';

    public function send(Order $order, $credentials = null)
    {
        $request = json_encode($this->prepareData($order));

        $curl = new Curl();
        $curl->reset();
        $curl->setOptions([
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $request,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($request)
            ]
        ]);
        $response = $curl->post(self::URL);

        if (!isset($response['success'])) {
            $reason = (isset($response['error_message'])) ? $response['error_message'] : '';
            throw new DeliveryException("Failed to send Order #$order->order_hash. Reason: $reason");
        }

        return $response['track_number'];
    }

    private function prepareData(Order $order)
    {
        $customer = $order->customer;
        return [
            'username' => self::USERNAME,
            'password' => self::PASSWORD,
            'method' => self::METHOD_CREATE_ORDER,
            'client_address_id' => 'ADDR3820497_4771',  //TODO: implement real address (should be set at personal page at Fetchr site)
            'pickup_location' => self::PICKUP_LOCATION,
            'pickup_country' => self::PICKUP_COUNTRY,
            'pickup_city' => self::PICKUP_CITY,
            'data' => [
                [
                    'order_reference' => $order->order_hash,
                    'name' => $customer->name,
                    'email' => $customer->email,
                    'phone_number' => $customer->phone_country_code . $customer->phone . $customer->phone_extention,
                    'address' => $customer->address,
                    'receiver_country' => $customer->country->country_name,
                    'receiver_city' => $customer->city->city_id,
                    'payment_type' => 'COD',
                    'amount' => $order->total_amount,
                    'description' => '-',
                    'comments' => '-'
                ]
            ]
        ];
    }

    public function getClassName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }
}