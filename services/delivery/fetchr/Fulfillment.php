<?php

namespace crm\services\delivery\fetchr;

use common\models\order\Order;
use common\services\delivery\DeliveryException;
use crm\services\delivery\DeliveryApiInterface;
use linslin\yii2\curl\Curl;

class Fulfillment implements DeliveryApiInterface
{
    private static const URL = 'http://bash.im/';
    private static const CREDENTIALS = [
        'uae' => [
            'username' => 'uae_username',
            'password' => 'uae_password'
        ],
        'bahrain' => [
            'username' => 'bahrain_username',
            'password' => 'bahrain_password'
        ],
        'ksa' => [
            'username' => 'ksa_username',
            'password' => 'ksa_password'
        ],
    ];

    /**
     * @param Order $order
     * @param $credentials
     * @return mixed
     * @throws DeliveryException
     */
    public function send(Order $order, $credentials)
    {
        $data = json_encode($this->prepareData($order, self::CREDENTIALS[$credentials]));

        $curl = new Curl();
        $curl->reset();
        $curl->setOptions([
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            ]
        ]);
        $response = $curl->post(self::URL);

        if (!isset($response['success'])) {
            $reason = (isset($response['error_message'])) ? $response['error_message'] : '';
            throw new DeliveryException("Failed to send Order #$order->order_hash. Reason: $reason");
        }

        return $response['tracking_no'];
    }

    private function prepareData(Order $order, $credentials)
    {
        $customer = $order->customer;
        return [[
            'order' => [
                'credentials' => [
                    'username' => $credentials['username'],
                    'password' => $credentials['password']
                ],
                'warehouse_location' => [
                    'address' => $customer->address,
                    'city' => $customer->city->city_name,
                    'country' => $customer->country->country_name,
                ],
                'details' => [
                    'discount' => '0',
                    'payment_method' => 'COD',
                    'order_id' => $order->order_hash,
                    'customer_firstname' => $customer->name,
                    'customer_lastname' => $customer->name,
                    'customer_phone' => $customer->phone,
                    'customer_email' => $customer->email,
                    'customer_address' => $customer->address,
                    'customer_city' => $customer->city->city_name,
                    'customer_country' => $customer->country->country_name,
                ],
                'items' => $this->getItems($order)
            ]
        ]];
    }

    private function getItems(Order $order)
    {
        $items = [];
        foreach ($order->orderSku as $order_sku) {
            $items[] = [
                'client_ref' => $order->order_hash,
                'name' => $order->offer->offer_name,
                'sku' => $order_sku->sku->sku_name,
                'quantity' => $order_sku->amount,
                'merchant_details' => [
                    'mobile' => $order->customer->phone,
                    'phone' => $order->customer->phone,
                    'name' => $order->customer->name,
                    'address' => $order->customer->address,
                ],
                'COD' => '0',
                'price' => $order_sku->cost,
                'is_voucher' => 'No',
            ];
        }
        return $items;
    }

    public function getClassName(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }
}