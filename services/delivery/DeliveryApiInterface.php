<?php

namespace crm\services\delivery;

use common\models\order\Order;

/**
 * Interface DeliveryApiInterface
 * @package crm\services\delivery
 */
interface DeliveryApiInterface
{
    /**
     * @param Order $order
     * @param $credentials
     * @return string|boolean
     */
    public function send(Order $order, $credentials);

    /**
     * @return string
     */
    public function getClassName();
}